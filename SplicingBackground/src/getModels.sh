#!/bin/bash

# Convert multi-exon Cufflinks-based gene models from
#   https://www.ncbi.nlm.nih.gov/pubmed/31191588
#   http://proteomics.ysu.edu/publication/data/Tomato/tomato.gff
# to bed-detail format for visualization and analysis in Integrated Genome Browser
#
# Requires:
#   git@bitbucket.org:lorainelab/genomesource.git
#   tabix and samtools bgzip

target="pubmed_id_31191588.bed.gz"
if [ ! -s data/$target ];
   then 
       tmp="tmp"
       gunzip -c data/tomato.gff3.gz | ./src/fixSplicingPaperGff.py > $tmp.gff
       simpleGffToBedDetail.py -g $tmp.gff -b $tmp.bed --multi_exon
       sort -k1,1 -k2,2n $tmp.bed | bgzip > data/$target
       tabix -s 1 -b 2 -e 3 data/$target
       rm $tmp.bed $tmp.gff
fi
