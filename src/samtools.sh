#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead
#
# to run:
#
# qsub-doIt.sh .sam samtools.sh >jobs.out 2>jobs.err  
#
cd $PBS_O_WORKDIR
module load samtools
samtools view -S -b $F > $S.unsorted.bam
samtools sort $S.unsorted.bam -o $S.bam
rm $S.unsorted.bam
samtools index $S.bam

