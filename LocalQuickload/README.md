Instructions
------------

*To upload the datasets in IGB, click "configure" to open "preferences" after opening the software. Click add and choose the LocalQuickload folder. 
*To add new datasets, edit the annots.xml file and add a title, description and public URL of the BAM file which you want to add.
